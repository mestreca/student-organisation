#include <stdio.h>
#include "header.h"



//Création d'un élément d'une liste de notes
T_Note *creerNote(float note, char *matiere){
    T_Note *newNote = malloc(sizeof(struct Note));
    if (newNote == NULL){
        printf("Erreur d'allocation !/n");
        return NULL;
    }
        newNote->note = note;
        newNote->matiere = matiere;
        newNote->suivant = NULL; //Par default pas de notes suivantes
    
    return newNote;
}

//Création d'un élément d'une liste d'étudiants
T_Etudiant *creerEtudiant(int idEtu, char *nom, char *prenom){
    T_Etudiant *newEtu = malloc(sizeof(struct Etudiant));
    if (newEtu == NULL){
        printf("Erreur d'allocation !/n");
        return NULL;
    }
    
    newEtu->idEtu = idEtu;
    newEtu->nom = nom;
    newEtu->prenom = prenom;
    newEtu->nb_note = 0;
    newEtu->moy = 0;
    newEtu->Liste_Note = NULL;
    newEtu->suivant = NULL; //Par default pas d'étudiants suivant
    
    return newEtu;
}

//Ajout d'une note en tête d'une liste de notes
T_ListeNotes ajouterNote(float note, char *matiere, T_ListeNotes listeNotes){
    T_Note *newNote = creerNote(note, matiere); //C'est un pointeur vers une Note
   
    newNote->suivant = listeNotes;
  
    return newNote;
  
}

//Ajout d'une note pour un étudiant dans une liste d'étudiant
T_ListeEtu ajouterNoteEtu(float note, char *matiere, int idEtu, T_ListeEtu listeEtu){
    
    if((idEtu < 0) || (note > 20.0) || (note < 0.0)){
        printf("ERREUR ID NEGATIF INTERDIT OU NOTE INVALIDE \n");
        return listeEtu; //On retourne la liste de base sans modifications
    }
    
    T_ListeEtu p_aux;
    p_aux = listeEtu;
    
    if(p_aux == NULL){ //Si la liste ne contient aucun étudiants
        printf("La liste est pour le moment NULL veuillez ajouter un étudiant en premier lieu !\n");
        return listeEtu;
    }
    
    while(p_aux->idEtu != idEtu){
        p_aux = p_aux->suivant;
        if(p_aux == NULL)
            break;
    }//On pointe sur l'étudiant qui nous interesse ou NULL si il n'existe pas encore
    
    //On doit vérifier dans la liste des matières de cet étudiant que la matière n'existe pas déjà
    T_ListeNotes p2_aux = p_aux->Liste_Note;
    
    while(p2_aux != NULL){
        if(strcmp(p2_aux->matiere, matiere)==0){ //La matière existe déjà pour cet étudiant
            printf("ERREUR LA MATIÈRE EXISTE DÉJÀ !\n");
            return listeEtu; //On retourne la listeEtu de base sans modification
        }
        p2_aux = p2_aux->suivant;
    }
    
    if(p_aux == NULL){ //l'étudiant n'existe pas encore => On doit le créer, on demande donc à l'utilisateur son nom et son prénom
        char *nom = malloc(30*sizeof(char)); //On alloue dynamiquement la mémoire
        char *prenom = malloc(30*sizeof(char));
        printf("##### Nous allons ajouter un nouvel élève #####\n Veuillez saisir son nom et prenom :\t");
        scanf("%s %s",nom,prenom);
        listeEtu =  ajouterEtu(idEtu, nom, prenom, listeEtu);
        p_aux = listeEtu;
        printf("id nouvel etu =%d et prenom = %s\n\n", p_aux->idEtu, p_aux->prenom);
   
    }
  
   
    p_aux->Liste_Note = ajouterNote(note, matiere, p_aux->Liste_Note);
    p_aux->nb_note ++;
    p_aux->moy = calculMoyenne(idEtu, listeEtu);
  
   
    return listeEtu;
   

}



//Afficher la liste des étudiants
void affiche_ListeEtu(T_ListeEtu listeEtu){
    T_ListeEtu p_aux = listeEtu;
    
    while(p_aux != NULL){
        printf("%s - %s d'id #%d\n",p_aux->nom,p_aux->prenom,p_aux->idEtu);
        
        T_ListeNotes p2_aux = p_aux->Liste_Note;
        if(p2_aux != NULL)
        printf("Sa liste de notes est : \n");
        while (p2_aux != NULL) {
            printf("Matiere : %s & note : %.2f\n", p2_aux->matiere,p2_aux->note);
            p2_aux = p2_aux->suivant;
        }
        printf("\n");
        p_aux = p_aux->suivant;
    }

    
}


//Suppression d'une note etu
T_ListeEtu supprimerNoteEtu(char *matiere, int idEtu, T_ListeEtu listeEtu){
    
    if(idEtu < 0){
        printf("ERREUR ID NEGATIF INTERDIT \n\n");
        return listeEtu;
    }
    
    T_ListeEtu p_aux = listeEtu;
    
    while(p_aux != NULL && p_aux->idEtu != idEtu){ //permet de prendre en compte si l'id n'existe pas
        
         p_aux = p_aux->suivant;
    }
    
    if(p_aux == NULL){
        printf("Erreur\n\n");
        return listeEtu;
    }
        
    T_ListeNotes p2_aux = p_aux->Liste_Note;
   
     
        while(strcmp(p2_aux->matiere,matiere) !=0){ //On avance jusqu'à la matière désirée
            p2_aux = p2_aux->suivant;
        }
    
    if (p2_aux == p_aux->Liste_Note) { //Si on pointe sur la première matière
        p_aux->Liste_Note = p2_aux->suivant;
         p_aux->nb_note --;
         p_aux->moy = calculMoyenne(idEtu, listeEtu);
    }
    
    else {
   
        T_ListeNotes p3_aux = p_aux->Liste_Note;
         p_aux->nb_note --;
         p_aux->moy = calculMoyenne(idEtu, listeEtu);
        
        while(p3_aux->suivant != p2_aux){
            p3_aux = p3_aux->suivant;
        }//Pointe sur le predecesseur de p2_aux(la matière à supprimer)
    
        p3_aux->suivant = p2_aux->suivant;
       
    }
    
   
    //Si étudiant n'a plus de note => On le supprime
    if (p_aux->nb_note == 0){
       listeEtu = supprimerEtu(idEtu, listeEtu);
    }
    
  
   
    return listeEtu;
    
}

    
//Suppression d'un étudiant
    T_ListeEtu supprimerEtu(int idEtu, T_ListeEtu listeEtu){
        T_ListeEtu p_aux = listeEtu;

        while(p_aux->idEtu != idEtu ){ //p_aux pointe vers l'étudiant à supprimer
            p_aux = p_aux->suivant;
        }
          T_ListeEtu p2_aux = listeEtu;
        
        if(p_aux == listeEtu){ //Quand il est premier
            listeEtu = p_aux->suivant;
          
          
        }
        
        else{
        
        while(p2_aux->suivant != p_aux){
            p2_aux = p2_aux->suivant;
        }//Pointe sur le predecesseur de p_aux(étudiant à supprimer)
        
        p2_aux->suivant  = p_aux->suivant; //On fait le raccord
        }
      
      
 return listeEtu;
}


//Ajouter un étudiant
T_ListeEtu ajouterEtu(int idEtu, char *nom, char *prenom, T_ListeEtu listeEtu){
    
    if(idEtu < 0){
        printf("ERREUR ID NEGATIF INTERDIT \n\n");
        return listeEtu;
    }
    //On vérifie d'abord que l'idEtu n'est pas déjà prit
    T_ListeEtu p_aux = listeEtu;
    while (p_aux != NULL) {
        if(p_aux->idEtu == idEtu){ //Si on trouve l'idEtu parmis a liste des étudiants alors on affiche un message d'erreur
            printf("ERREUR -> cet id est déjà prit ! \n\n");
            return listeEtu;
        }
        p_aux = p_aux->suivant;
    }
    
    T_Etudiant *newEtu;
   
    newEtu = creerEtudiant(idEtu, nom, prenom);
    
   
    newEtu->suivant = listeEtu;
    
     listeEtu = newEtu;

   
    return listeEtu;
}

//Affichage du classement par ordre décroissant de la moyenne
void afficherClassement(T_ListeEtu listeEtu){
//printf("\n------------ Affichage du classement -------------\n");
    T_ListeEtu p_aux = listeEtu;
   
    
    //Rajouter tableau de booléen
  
  
    if(p_aux == NULL){ //Si la liste est vide On renvoie directement "fin de la liste"
        printf("-- Fin de la liste --\n\n");
        
    }
    
    else {
    
    int nbEtu = 0;
    
    while (p_aux != NULL) { //On compte le nombre d'étudiant présent dans notre liste pour allouer la mémoire en fonction
        p_aux = p_aux->suivant;
        nbEtu++;
    }
    
  
        T_ListeEtu *tabEtu = malloc(nbEtu*sizeof(T_ListeEtu)); //Tableau de nos étudiants
        p_aux = listeEtu; //On "replace le curseur" au niveau de la tête de la liste
        
        int i,j;
        
        for(i=0; i<nbEtu; i++){ //On remplit le tableau
            tabEtu[i] = p_aux;
            p_aux = p_aux->suivant;
        }
        
        for (i=0; i<nbEtu; i++) {
            for (j=i; j<nbEtu; j++) {
                if(tabEtu[j] < tabEtu[i]){ //On trie le tableau par ordre décroissant (en inversant les valeurs si besoin)
                    p_aux= tabEtu[i];
                    tabEtu[i] = tabEtu[j];
                    tabEtu[j] = p_aux;
                }
            }
        }
        
        for(i = 0; i < nbEtu; i++){
            printf("%.2f %s %s\n",  tabEtu[i]->moy, tabEtu[i]->nom, tabEtu[i]->prenom); //On affiche finalement les éléments du tableau trié
        }
        printf("-- Fin de la liste --\n");
       
    }
   
}
    


float calculMoyenne(int idEtu, T_ListeEtu listeEtu){
    T_ListeEtu p_aux = listeEtu;
    
    if(idEtu < 0){
        printf("ERREUR ID NEGATIF INTERDIT \n\n");
        exit(0);
    }
    
    while(p_aux->idEtu != idEtu ){ //p_aux pointe vers l'étudiant dont on veux calculer la moyenne
        p_aux = p_aux->suivant;
    }
    
    if(p_aux->nb_note == 0){
       // printf("Erreur l'étudiant ne possède pas de notes \n");
        return 0;
       
    }
    
    else{
        T_ListeNotes p2_aux = p_aux->Liste_Note;
        float somme =0.0;
        while (p2_aux != NULL) {
            somme += p2_aux->note;
           
            p2_aux = p2_aux->suivant;
        }
        p_aux->moy = somme/(float)p_aux->nb_note;
      
    }
    
    return p_aux->moy;
    
}

void sousListes(char* matiere, T_ListeEtu listeEtu){
printf("\n------------ Affichage des sous listes -------------\n");
T_ListeEtu p_aux = listeEtu;
T_ListeNotes p2_aux = p_aux->Liste_Note;

     T_ListeEtu listeSucces = NULL;
     T_ListeEtu listeEchecs = NULL;
     T_ListeEtu listeNonInscrits = NULL;
  
while(p_aux != NULL){ //On parcours notre liste d'étudiants
    p2_aux = p_aux->Liste_Note;
    
    if(p2_aux == NULL){//Cas où l'étudiant ne possède pas encore de matieres -> on le range dans la liste des non inscrits à l'UV
         listeNonInscrits = ajouterEtu(p_aux->idEtu, p_aux->nom, p_aux->prenom, listeNonInscrits);
        p_aux = p_aux->suivant;
        p2_aux = p_aux->Liste_Note;
    }
        
    while (strcmp(p2_aux->matiere, matiere) != 0) { //On cherche la matière désirée
        p2_aux = p2_aux->suivant;
        if(p2_aux == NULL) //Si l'étudiant ne possède pas la matière alors on sort de la boucle à l'aide d'un break
            break;
    }
    if (p2_aux != NULL){ // L'étudiant pointé par p_aux possède cette matiere
        
        if(p2_aux->note >= 10.0){ //On considère que l'étudiant réussi l'UV si il a une moyenne supérieur ou égal à 10
            listeSucces = ajouterEtu(p_aux->idEtu, p_aux->nom, p_aux->prenom, listeSucces);
        }
    
        else{ //L'étudiant à loupé l'UV
            listeEchecs = ajouterEtu(p_aux->idEtu, p_aux->nom, p_aux->prenom, listeEchecs);
        }
    }
    else{ //L'étudiant ne fait pas cette matière
        listeNonInscrits = ajouterEtu(p_aux->idEtu, p_aux->nom, p_aux->prenom, listeNonInscrits);
    }
    
    p_aux = p_aux->suivant;

  
}

    printf("#Les étudiants qui ont réussi l'UV sont : \n");
    affiche_ListeEtu(listeSucces);
    
    
    printf("#Les étudiants qui ont échoué à l'UV sont : \n");
    affiche_ListeEtu(listeEchecs);
    
    
    printf("#Les étudiants qui ne sont pas inscrit à l'UV sont : \n");
    affiche_ListeEtu(listeNonInscrits);
    
    printf("\n\n-- Fin sous listes --\n\n ");
    
   
}



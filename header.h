
#ifndef header_h
#define header_h
#define TAILLE_CHAR 30
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//Penser à faire une fonction afficher pour chaque ajout d'etudiant et de note

struct Note{ //T_Note
    
    float note;
    char *matiere;
    struct Note *suivant;
    
};

typedef struct Note T_Note;
typedef struct Note *T_ListeNotes;

struct Etudiant{ //T_ListeEtu
    
    int idEtu;
    char *nom;
    char *prenom;
    int nb_note;
    T_ListeNotes Liste_Note;
    float moy;
    struct Etudiant *suivant;
    
};
typedef struct Etudiant T_Etudiant;
typedef struct Etudiant *T_ListeEtu;


T_Note *creerNote(float note, char *matiere);
T_Etudiant *creerEtudiant(int idEtu, char *nom, char *prenom);
T_ListeNotes ajouterNote(float note, char *matiere, T_ListeNotes listeNotes);
T_ListeEtu ajouterNoteEtu(float note, char *matiere, int idEtu, T_ListeEtu listeEtu);
void affiche_ListeEtu(T_ListeEtu listeEtu);
T_ListeEtu supprimerNoteEtu(char *matiere, int idEtu, T_ListeEtu listeEtu);
T_ListeEtu ajouterEtu(int idEtu, char *nom, char *prenom, T_ListeEtu listeEtu);
T_ListeEtu supprimerEtu(int idEtu, T_ListeEtu listeEtu);
float calculMoyenne(int idEtu, T_ListeEtu listeEtu);
void afficherClassement(T_ListeEtu listeEtu);
void sousListes(char* matiere, T_ListeEtu listeEtu);


#endif /* header_h */

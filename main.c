#include <stdio.h>
#include "header.h"

int main(int argc, const char * argv[]) {
    
    T_ListeEtu listeEtu = NULL;
    
    /*listeEtu = ajouterEtu(12, "Christian","Touzeau", listeEtu);
    listeEtu = ajouterEtu(10, "Valentin","Mestre", listeEtu);
     listeEtu = ajouterEtu(13, "André","Malraux", listeEtu);
  
    
    listeEtu = ajouterNoteEtu(12.34, "NF16", 12, listeEtu);
    listeEtu = ajouterNoteEtu(9.6, "MT09", 13, listeEtu);
    listeEtu = ajouterNoteEtu(11.3, "MT09", 10, listeEtu);*/
  
 
  
    
    printf("#################################################################\n# Bienvenue dans l'Application du TP3 - NF16 par TOUZEAU/MESTRE #\n#################################################################\n");
  
    int choix_menu =0; //entier qui nous sert pour notre menu de séléction des actions
    
    while(1){
        printf("\n### Quelle action souhaitez vous réaliser ? ###\n\n");
        printf("1. Creer/Continuer la liste d'étudiants\n");
        printf("2. Ajouter une note\n");
        printf("3. Supprimer une note\n");
        printf("4. Afficher la liste des etudiants\n");
        printf("5. Afficher les sous listes\n");
        printf("6. Afficher le classement (par ordre décroissant des moyennes)\n");
        printf("7. Quitter l'application\n\nChoix :\t");
        scanf("%d",&choix_menu);
        
        switch(choix_menu){
            case 1:
            {
                    int idEtu= 0;
                    char* nom = (char*)malloc(TAILLE_CHAR*sizeof(char));
                    char* prenom = (char*)malloc(TAILLE_CHAR*sizeof(char));
                    
                    printf("Saisissez l'id, le nom et le prenom de l'etudiant à ajouter :\t");
                    scanf("%d %s %s",&idEtu, nom, prenom);
                    
                    listeEtu = ajouterEtu(idEtu, nom, prenom, listeEtu);
             
                break;
            }
                
            case 2 :
            {
                float note;
                char* matiere = (char*)malloc(6*sizeof(char));
                int idEtu = 0;
                
                printf("Saisissez l'idetu de l'etudiant en question :\t");
                scanf("%d", &idEtu);
                printf("Saisissez l'UV :\t");
                scanf("%s", matiere);
                printf("Saissez la note :\t");
                scanf("%f", &note);
                
                listeEtu = ajouterNoteEtu(note, matiere, idEtu, listeEtu);
            
                break;
            }
                
            case 3 :
            {
                int id = 0;
                char* matiere = (char*)malloc(6*sizeof(char));
                
                printf("Saisissez l'id et la matiere a supprimer :\t");
                scanf("%d %s", &id, matiere);
                printf("\nVous souhaitez supprimer la matière %s ! \n", matiere);
            
                listeEtu = supprimerNoteEtu(matiere, id, listeEtu);
                free(matiere);
                break;
            }
                
            case 4 :
               affiche_ListeEtu(listeEtu);
                break;
                
            case 5 :
            {
                char* matiere = (char*)malloc(6*sizeof(char));
                printf("Entrez la matière de la sous liste que vous voulez affichez :\t");
                scanf("%s", matiere);
                
                sousListes(matiere, listeEtu);
              
                break;
            }
                
            case 6 :
                afficherClassement(listeEtu);
                break;
            
                
             default: //On va libérer toute la mémoire utilisé
            {
                T_ListeEtu temp = NULL;
                T_ListeNotes temp2 = NULL;
                while (listeEtu != NULL){
                    temp = listeEtu;
                    while (listeEtu->Liste_Note != NULL) {
                        temp2 = listeEtu->Liste_Note;
                        listeEtu->Liste_Note = listeEtu->Liste_Note->suivant;
                        free(temp2);
                    }
                    listeEtu = listeEtu->suivant;
                    free(temp);
                }
    
                return 0;
            }
            
        }//End switch
                
    } //End menu (boucle while)
    

    return 0;
}
